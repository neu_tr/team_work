package net.sxbang.service;

import net.sxbang.dao.UserDAO;
import net.sxbang.dao.UserDAOImpl;
import net.sxbang.model.User;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO = new UserDAOImpl();

	/* (non-Javadoc)
	 * @see net.sxbang.service.UserService#login(java.lang.String, java.lang.String)
	 */
	@Override
	public String login(String username, String password) {
		// andy on 2018
		System.out.println(this.getClass().getName() + "login");
		User user = userDAO.findByUsername(username);
		if (user != null) {
			return "登录成功";
		}
		return "登录失败";
	}
	
	@Override
	public String register(String username, String password) {
		System.out.println(this.getClass().getName() + "register");
		User user = userDAO.findByUsername(username);
		if (user == null) {
			user = new User();
			user.setPassword(username);
			user.setPassword(password);
			userDAO.save(user);
			return "注册成功";
		}
		return "已存在用户";
	}
}
