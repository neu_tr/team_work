package net.sxbang.controller;

import net.sxbang.service.UserService;
import net.sxbang.service.UserServiceImpl;

public class UserController {
	
	private UserService userService = new UserServiceImpl();
	
	public void login(String username, String password) {
		userService.login(username, password);
	}
	
	public void register(String username, String password) {
		userService.register(username, password);
	}
}
